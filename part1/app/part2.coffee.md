Create and initialize our Logo class.

    'use strict'

    class Logo
      constructor: (target) ->



Obtain our canvas element and drawing context. Consequently, draw a circle and initialize our animation.

        size = 450
        @ctx = @createCanvas(target, size).getContext('2d')
        @del = yes

        @paint()
        size /= 2

        @ctx.arc size, size, size, 0, (Math.PI * 2), no
        @ctx.fill()

        @reset()
        @start()



Create our main coordinates array programatically.

      createArray: (wmin, wmax, height, arr=[]) ->
        line = (w, h, asc=yes, series=[]) ->
          series.push [
            if asc then w + (i * 20) else w - (i * 20)
            h + (i * 10)
          ] for i in [0...6]

          return series

        for i in [0...4]
          even = (i % 2) is 0
          width = if even then wmin else wmax

          arr.push (line width, height + i * 60, even)

        return arr



Create the canvas element and append it to the document.

      createCanvas: (target, size) ->
        canvas = document.createElement('canvas')
        canvas.width = size
        canvas.height = size

        document.body.querySelector(target).appendChild canvas
        return canvas



Determines which color to use to paint the upcoming shape.

      paint: ->
        @ctx.fillStyle = switch
          when @del then '#e83939'
          when (Math.round Math.random()) % 2 is 1 then '#F5ADAD'
          else '#fff'

        return



Resets (or initializes) our coordinates array. Also inverts our `@del` variable, which is used to determine if the animation should be 'painting' or 'erasing' items from the animation.

      reset: ->
        @arr = @createArray(180, 280, 100)
        @del = (not @del)

        return



Go through each item inside `@arr` and use it as coordinates to draw on the canvas every 150ms. Note the use of `requestAnimationFrame()`, which is much more perfomant than the original `setInterval()`.

      start: =>
        ding = no

        for series, s in @arr
          for item, i in series
            continue unless item?
            ding = yes

            @paint()
            @ctx.fillRect item[1], item[0], 20, 20
            delete @arr[s][i]
            break

          break if ding

        setTimeout =>
          requestAnimationFrame @start
          return
        , (1000 / 15)

        @reset() unless ding
        return



Initialize our animation.

    new Logo('aside.anim')
