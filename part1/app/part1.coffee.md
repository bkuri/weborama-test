Define our variables. Our `growing` var is used to short-circuit the function if there's already an animation in progress.

    'use strict'

    S = (Snap '#svg2')
    area = S.select('#area')
    banner = document.querySelector('aside.banner')
    features = S.select('#features')
    growing = no
    logo = S.select('#logo')
    overlay = S.select('#overlay')
    spirals = S.select('#spirals')



Used to grow/shrink the item defined by `what`. If `scale === 1` then we revert to our original dimensions.

    grow = (what, scale=1) ->
      bbox = what.getBBox()
      growing = yes

      unless scale is 1
        transform = new Snap.Matrix()
        transform.scale scale, scale, bbox.cx, bbox.cy
        transform.add what.transform().localMatrix

      else transform = (what.attr 'state-static')

      what.animate {transform}, 200, mina.easeout, ->
        growing = no
        return

      return



Called when the pointer is over the banner, but it may also be called when a hover state is not available and the banner has been clicked instead. This function also uses `screenad` to broadcast an event, start a timer, and finally resize and clip the host iframe.

    hover = ->
      return if growing
      grow features, 0.65
      grow logo, 1.5

      banner.classList.add 'hover'
      overlay.animate transform: 'T-200,0', 200, mina.easeout

      screenad.event 'banner_expand'
      screenad.startTimer 'banner_hover'
      screenad.resize '728', '350'
      screenad.setClip 0, 0, 728, 350
      screenad.position()
      return



Reset all items to their default state. Also broadcasts `screenad`'s collapse event, stops the timer, and resets host iframe dimensions.

    reset = ->
      grow features
      grow logo

      banner.classList.remove 'hover'
      overlay.animate transform: 'T100,0', 200, mina.easeout

      screenad.event 'banner_collapse'
      screenad.stopTimer 'banner_hover'
      screenad.resize '728', '90'
      screenad.setClip 0, 0, 728, 90
      screenad.position()
      return



Determine whether we should be expanding or triggering a click.

    area.click ->
      if document.querySelector('aside.banner.hover')?
        screenad.click 'default'
        reset()
      else hover()
      return



Trigger a hover or reset state.

    area.hover hover, reset



Animate the features wheel and background spiral.

    Snap.animate 0, 1, ->
      features.rotate 0.125
      spirals.rotate 0.05
      return
