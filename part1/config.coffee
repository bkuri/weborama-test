# change your local variables here

locals =
  charset: 'utf-8'
  lang: 'en'
  name: 'weborama-assignment-part1'
  title: 'Weborama JS dev assignment'

  urls:
    app: '/js/app.js'
    brunch: 'config.coffee'
    coffee: 'app/app.coffee'
    css: '/css/app.min.css'
    font: 'https://fonts.googleapis.com/css?family=Rajdhani'
    jade: 'app/index.jade'
    screenad: '//media.adrcdn.com/scripts/screenad_interface_1.0.3_scrambled.js'
    styl: 'app/styles/app.styl'
    vendor: '/js/vendor.js'



# Brunch config.
# See http://brunch.io/#documentation

exports.config =
  conventions:
    assets: /^app\/assets\//

  files:
    javascripts:
      joinTo:
        'js/app.js': /^app/
        'js/vendor.js': /^(vendor)/

    stylesheets:
      joinTo:
        'css/app.min.css': /.*/

      order:
        before: ['app/app.css']

    templates:
      joinTo:
        '.compile-jade': /^app/

  modules:
    definition: no
    wrapper: no

  optimize: yes

  overrides:
    minifier:
      sourceMaps: no

      plugins:
        pleeease:
          optimizers:
            minifier: yes
            mqpacker: yes

  paths:
    jadeCompileTrigger: '.compile-jade'

  plugins:
    cleancss:
      keepSpecialComments: 0
      removeEmpty: yes

    DoccoRunner:
      path: '../part2/part2.coffee.md'

    jadePages:
      jade:
        locals: locals

    stylus:
      plugins: [
        'autoprefixer-stylus',
        'axis'
      ]

    uglify:
      compress:
        global_defs:
          DEBUG: no

      mangle: no
