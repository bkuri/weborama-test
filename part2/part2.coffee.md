Weborama JS Dev Assignment
==========================

Part 2: Refactoring
-------------------

### Introduction

The file you are currently reading serves both as the documentation and the source code for Part 2 of the assignment. It has been written using literate Coffeescript. Please refer to the 'Usage' section below to learn how to process this file. There is also a 'Notes' section at the end of this document.



### Requirements

The following tools are required in order to compile, lint, and compress this file:

  * [Coffeescript](http://coffeescript.org)
  * [JSHint](http://jshint.com)
  * [UglifyJS2](http://lisperator.net/uglifyjs/)



### Usage

This file can be compiled to standard JS with this command:
```coffee -c part2.coffee.md```

The compiled JS can then be checked with JSHint with this command:
```jshint --verbose --show-non-errors --filename part2.js```

Aditionally, the compiled JS can be minimized by using UglifyJS. The following command produced the best results in our case:
```uglifyjs --mangle --mangle-props --reserved-file .reserved.json --screw-ie8 --compress negate_iife part2.js -o part2.min.js```

The file can be optionally gzipped for an even smaller file size like so:
```gzip -k9 part2.min.js```



### Benefits over the original version

  * Clean, modular, succint code.

  * JSHint compatible.

  * Uses ES5 strict mode.

  * Around 700 bytes gzipped.

  * Fully documented alongside the code.

  * The original array used 3 dimensions and strings to denote our coordinates, which is a pretty expensive way of storing our data. Our improved array is only two-dimensional and returns numbers out of the box, which are much faster to use and manipulate (i.e. no need to do `String.split()` anymore).

  * Uses coffeescript 'classes', which are actually prototypes under the hood. This tends to use less resources than other methods.

  * Does not pollute the global namespace.

  * Extra finishing animation as a bonus.



### Code

Create and initialize our Logo class.

    'use strict'

    class Logo
      constructor: ->



Obtain our canvas element and drawing context. Consequently, draw a circle and initialize our animation.

        size = 450
        @ctx = @createCanvas(size).getContext('2d')
        @del = yes

        @paint()
        size /= 2

        @ctx.arc size, size, size, 0, (Math.PI * 2), no
        @ctx.fill()

        @reset()
        @start()



Create our main coordinates array programatically.

      createArray: (wmin, wmax, height, arr=[]) ->
        line = (w, h, asc=yes, series=[]) ->
          series.push [
            if asc then w + (i * 20) else w - (i * 20)
            h + (i * 10)
          ] for i in [0...6]

          return series

        for i in [0...4]
          even = (i % 2) is 0
          width = if even then wmin else wmax

          arr.push (line width, height + i * 60, even)

        return arr



Create the canvas element and append it to the document.

      createCanvas: (size) ->
        canvas = document.createElement('canvas')
        canvas.width = size
        canvas.height = size

        document.body.appendChild canvas
        return canvas



Determines which color to use to paint the upcoming shape.

      paint: ->
        @ctx.fillStyle = switch
          when @del then '#a00'
          when (Math.round Math.random()) % 2 is 1 then '#fff'
          else '#f99'

        return



Resets (or initializes) our coordinates array. Also inverts our `@del` variable, which is used to determine if the animation should be 'painting' or 'erasing' items from the animation.

      reset: ->
        @arr = @createArray(180, 280, 100)
        @del = (not @del)

        return



Go through each item inside `@arr` and use it as coordinates to draw on the canvas every 150ms. Note the use of `requestAnimationFrame()`, which is much more perfomant than the original `setInterval()`.

      start: =>
        ding = no

        for series, s in @arr
          for item, i in series
            continue unless item?
            ding = yes

            @paint()
            @ctx.fillRect item[1], item[0], 20, 20
            delete @arr[s][i]
            break

          break if ding

        setTimeout =>
          requestAnimationFrame @start
          return
        , (1000 / 15)

        @reset() unless ding
        return



Initialize our animation.

    new Logo()



### Notes

  * I added an 'erasing' animation at the end of each loop which looks a bit nicer (IMO). Plus it saves us from drawing a new circle after every loop.

  * I decided to increase the animation speed to 15fps (hope that's OK).

  * My `.jshintrc` file supresses a warning (W041), which enforces strict equality when comparing to `null`. It can be easily 'fixed' in the compiled source, but it makes no difference whatsoever in the result.



### Conclusion

Thanks again for the oportunity to try out for the position. You can contact me [on twitter](http://twitter.com/bkuri) & [via email](mailto:weborama@bkuri.com).
