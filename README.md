Weborama JS Dev Assignment
==========================

https://bkuri.github.io/weborama-test/


Layout
------

The file layout is pretty straightforward:
  * Files inside `/final` contain the templates needed to build our html files.
  * Files contained in `/part1` make up a node development server which was used to develop the banner and website.
  * Files contained in `/part2` are related to solving the second part of the assignment.
  * Original schematics and Inkscape files go in `/misc`. This folder can be safely ignored.


Dev server installation
-----------------------

In order to use the dev server, you need to do the following:

  + Clone this repo: `$ git clone https://github.com/bkuri/weborama-test.git`
  + Install npm: `$ npm install`
  + Start the web server: `$ npm start`

The server should now be [running on this address](http://localhost:3333).


Compiling html files
--------------------

File compilation is accomplished using a [Cakefile](http://coffeescript.org/#cake). Simply run the following command to build a fresh set of files:
`$ cake build`

Do note that this will only work after running `npm install` (see above).


Thank you!
----------
